# Explain the permissions!!

There are two levels of permissions, the wiki permissions that are per-wiki and
the overall permissions that are for sysadmin type things.

## Wiki Permissions

Each wiki can have:

- An `owner` who has view and edit permissions and can grant or revoke view and
  edit permissions to others. They can also change what plugins the wiki uses
  and some other admin permissions that relate to the wiki.
- A list of `viewers` who can view the wiki
- A list of `editors` who have edit permissions

### Level-based Permissions

You can also set permissions based on authentication levels. Each login is
given an authentication level, currently they are `Guest`, `Normal` and
`Admin`. Each wiki can set which permissions are available on that wiki to each
login level.

- `view` - the person can view the wiki.
- `edit` - the person can make changes to the wiki.
- `admin` - the person can make changes to the wiki settings, make new wikis,
  make single file wikis from all or some of the wikis tiddler, stop the wiki
  server
- `script` - the person can use the wiki to trigger shell scripts
- `owner` - reserved for only the owner of the wiki. It grants the same
  privlidges as `view` and `edit` in addition to the `unloadWiki` message.
- `upload` - a special privlidge that lets you upload non-tiddler files (like
  media files)

A wiki has an owner. The owner always has view, edit and admin permissions.

The `Guest` login can never have admin or script permissions and can never own a
wiki.

## Admin Permissions

These permissions are for setting ownership and permissions.

They aren't used yet because I haven't made tools for this part yet

What do they need to be?

`Guest` - logged in as a guest
`Normal` - logged in with a normal name and password
`Admin` - logged in as an administrator

So far we have `Guest` who isn't allowed to change any wiki permissions unless
they own a wiki and `Admin` who can change any wiki permissions.

But without the utilities to change the permissions they don't mean much.
