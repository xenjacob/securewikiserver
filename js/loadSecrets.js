const settings = require('../LoadConfig.js')
const fs = require('fs')
const path = require('path')

// This doesn't get a try block, if it fails it should crash.
var key = fs.readFileSync(path.join(require('os').homedir(), settings.tokenPrivateKeyPath))

module.exports = key
