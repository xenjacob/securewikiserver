/*
  This is what you use to add a new person to the server. It adds them with a
  name and password.

  So we should make a way for people to change their passwords after they
  authenticate.

  Usage:

  node addperson.js name password level

  name and password are required, level is optional and defaults to guest

  Stuff this shows:
  - Getting and using commandline arguments
  - Hash passwords using bcrypt
  - Create folders if they don't already exist
  - Save information to a file and set appropriate file permissions
*/

var fs = require('fs')
var path = require('path')
var os = require('os')
var bcrypt = require('bcrypt')

var PouchDB = require('pouchdb')
var peopleDB = new PouchDB('people')

var settings = require('./LoadConfig.js')

if (require) {
  if (require.main === module) {
    if (process.argv.length < 3) {
      console.log('Usage: node ./addperson.js name password level')
    } else {
      var name = process.argv[2]
      var password = process.argv[3]
      var level = 'guest'
      if (process.argv.length >= 4) {
        level = process.argv[4]
      }
      var update = false
      if (process.argv.length >= 5) {
        if (process.argv[5]) {
          update = true
        }
      }

      saveCredentials(name, password, level, update)
    }
  }
}

function saveCredentials (name, password, level, update, oldPassword) {
  // Check to make sure that the persons info doesn't already exist on the
  // server
  peopleDB.get(name).catch(function(err) {
    if (err.name == 'not_found') {
      return {
        '_id': name,
        'level': level
      }
    } else {
      throw err
    }
  }).then(function(personData) {
    if (typeof personData._rev === 'undefined' && !update) {
      // A new person to add
      personData.hash = bcrypt.hashSync(password, Number(settings.saltRounds))
      return peopleDB.put(personData)
    } else if (typeof personData._rev !== 'undefined' && update) {
      var match = bcrypt.compareSync(oldPassword, personData.hash)
      if (match) {
        // Update the existing person
        personData.hash = bcrypt.hashSync(password, Number(settings.saltRounds))
        personData.level = level
        return peopleDB.put(personData)
      } else {
        throw 'Passwords do not match'
      }
    } else if (update && personData._rev !== 'undefined') {
      throw 'Person Exists'
    } else {
      throw 'No Person To Update'
    }
  }).catch(function(err) {
    if (err === 'Person Exists') {
      console.log('Person exists, can\'t make two people with same name')
    } else if (err === 'No Person To Update') {
      console.log('Can\'t update a person who doesn\'t have an entry')
    } else if (err === 'Passwords do not match') {
      console.log('Old password did not match')
    } else {
      console.log(err)
    }
  })
}

module.exports = saveCredentials
